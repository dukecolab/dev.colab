//Including shorthand.js for simplicity. try to keep it updated!
	//Shorthand.js
	//A library for simplifying the beauty that is vanilla JS
	//Developed by Toolbox
	
	var w = window;
	var d = document;
	d.id = d.getElementById;
	d.class = d.getElementsByClassName;
	d.name = d.getElementsByName;
	d.tag = d.getElementsByTagName;
	
	d.$ = d.querySelectorAll;
	
	d.on = d.addEventListener;

//ensure colab exists as a variable
if (typeof colab === "undefined") {var colab = {};}








colab.dashboard = (function() {
	//////////////////////////////
	//function definitions below//	
	//////////////////////////////
	
	"use strict";
	
	//function to generate unique ids for all objects
	id.current = 0;
	function id() {
		return id.current++;
	}
	
	
	
	
	
	
	
	
	
	
	///////////////////////////////
	//Meat! (Actual running code)//
	///////////////////////////////
	
	//loadCache();
	//pull last state from cache
	
	//fullUpdate();
	//update all objects asynchronously
		//saveCache();
		//when finished, save to cache (as part of the standard update function)
	
	
	//hold for the DOM to finish loading
	d.on('DOMContentLoaded',function() {
		//renderNav();
		//load up nav menu
		
		//load default object, detail view
		//render all status bar objects, summary view
		//render updates bar
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/////////////////////////////////////////////////////
	//example/base object for inheritance and reference//
	/////////////////////////////////////////////////////
	function ob(dash) {
		//renders the object in html, returning a node or array of nodes
		this.render = function(mode) {
			var ret = d.createElement('div');
			ret.innerHTML = "I AM A BASE OBJECT EXAMPLE"; 
			return ret;
		};
		
		//checks for updates, returns them in an array
		this.checkUpdates = function() {return ["base object created","other updates"];};
		
		//adds/lists/removes children of the object
		this.children = [];
		this.add = function(other) {this.children.push(other.id);};
		this.remove = function(other) {
			if (this.children.indexOf(other) > -1) {
				this.children.splice(this.children.indexOf(other),1);
			}
		};
		
		//unique id, uses a dashboard standard function
		this.id = dash.id();
		
		//metadata
		this.type = 'ob';
		this.description = "An example object";
		this.author = "meeee";
		this.uses = ["stuff","different stuff","a third thing"];
	}
	
	
	return {
		'id': id,
		'ob': ob
	};
	
})();