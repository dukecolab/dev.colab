## Developing for the Dev Dashboard ##

Hello future person! It's me, Toolbox! Apparently this platform has been useful enough to have someone want to build something for it! That's exciting. I spent many hours trying to figure out how to make this extensible, so you'll have to see how well I did. Good luck!

#### The Paradigm ####

The Dashboard is almost entirely frontend, with just settings and a few odds and ends being processed serverside. It also is entirely contained in one page. I know many times I've argued against blanket use of AJAX, but often times it can be useful. Hopefully this is one of those times. This allows it to leverage the client's processing and interface in a standard way with many existing services. It is focused around the concept of objects (a superset of javascript objects). These objects may own other objects, display themselves in a few ways, offer themselves up to be owned by other objects, produce updates, and have settings. For example, a Project is an object, as is a Team, or an API Key. Each of these can be seen a few different ways within the Dashboard.

#### The Nitty Gritty ####

Objects each have a standard interface that allows them to be used by the Dashboard at large. The most critical file for an Object is 'index.js', contained within its object folder. This index.js is run when the page is loaded, and defines the object for the Dashboard to use. A javascript object is created using a standard constructor format. Below we have an example of an object definition (for a 'team'):

```
//create the array to hold teams and add it as a new type of object for the dashboard
colab.dashboard.teams = [];
colab.dashboard.addType('team',colab.dashboard.teams);

document.addEventListener('dashboardUpdate',function() {
	"use strict";
	//This is where I would check for new teams, and then create them
	var team = new colab.dashboard.team(params);
	colab.dashboard.teams.push(team);
};

colab.dashboard.team = function(parameters) {
	"use strict";

	//renders the object in html, returning a node or array of nodes
	this.render = function(mode) {
			var ret = document.createElement('div');
			ret.innerHTML = "I AM A TEAM!"; 
			return ret;
	}
	
	//checks for updates, returns them in an array
	this.checkUpdates = function() {return ["stuff","otherstuff"];}
	
	//adds/lists/removes children of the object
	this.children = [];
	this.add = function(other) {this.children.push(other.id);}
	this.remove = function(other) {
		if (this.children.indexOf(other) > -1) {
			this.children.splice(children.indexOf(other),1);
		}
	}
	
	//unique id, uses a dashboard standard function
	this.id = dash.id();
	
	//metadata
	this.type = 'team';
	this.description = "A team for being in and doing things";
	this.author = "meeee";
	this.uses = ["colab-sbx-21","db.colab.duke.edu","flare guns"];
}

//lists possible child objects and allows objects to register themselves as potential children
colab.dashboard.team.addable = [];
colab.dashboard.team.accept = function(otherConstructor) {addable.push(otherConstructor);}
```	

Dashboard.js has a base ob constructor for you to use for inheritance and reference. 

As you can see, each object has several standard interfaces to be used. I'll just go from the top to bottom. 

colab.dashboard.addType registers a new array that would hold all of the objects of that type. This way, the dashboard can quickly iterate through all of its objects and list them by type.

The dashboardUpdate event is called periodically as an opportunity for new objects to be created as necessary. This is handled in the 

colab.dashboard.team is defined as a function, which we can think of as a constructor. The correct method of creating a new team is:
	var teamRocket = new team(parameters);
	colab.dashboard.teams.push(teamRocket);
This generates a new team object based on the parameters, stored in teamRocket. teamRocket is then added to the array of teams (which is tracked by the dashboard at large).

the render function can be used like teamRocket.render('summary'). This function produces the actual html representation of the object. It can be passed either 'summary','detail', or 'child', representing the requested type of display: a detailed full view of the object, a summary of the object, used in lists of all objects of that type, and a child view of the object, used when displayed as a child of another. Each should be rendered according to the html/css guidelines below.

checkUpdates is a function provided to the dashboard framework so that it can manually request updates regarding individual objects and keep the dom up to date. It might or might not need to then re-render the content (depending on whether it's currently being displayed).

children holds references to each of the child objects, which are added using the add function and removed with remove(). These should almost always be similar to the functions included here.

id is used as a unique identifier for all objects, often useful for stuff, and things

type is an easy way of evaluating the type of this object, and the rest of the metadata is mandatory data used by other objects or itself in order to properly list or render it.

addable is a list of the constructors of objects that can be added to this one. accept is used like so:
	colab.dashboard.team.accept(colab.dashboard.project);
This change will allow all teams to now accept projects as children, and give clues to the renderer that it should offer the ability to add that object.


##### HTML/CSS Guide #####

This is where I get nitpicky about the way that you write your html/css. I promise it helps keep things clean and logical, and makes further development and UI consistency both easier. I'll just keep it as a set of bullet points and add to it as I think of things!
	
	-Inside object rendering, h2 should be the max header for detail, and h3 for summary/child
	-there are standard classes in the css for each render type .detail, .summary, .child. Use them to wrap the entire rendering
	-You can embed your own javascript, html, and css in every single rendering, it's true. Just thing about whether or not it hurts the encapsulation of the object or will make it hard to write new objects in the future that might use yours or yours might use
	-I thought I had more but whatever.

##### data.js and data storage #####

data.js is a server-generated file that contains many useful pieces of data, as well as the overall settings for the user. The data is all contained within colab.dashboard.user, and an example object is shown below.

{
	'netid': 'jpo3',
	'email': 'jpo3@duke.edu'
	'affiliation': 'student',
	
}

Another thing to keep in mind when designing for the Dashboard is that, to the best of its ability, it caches the objects that are created