/* global colab.data from data.js */
/* global d from shorthand.js */
/* global w from shorthand.js */
//TODO: MAKE EXTENSIBLE
if (document.readyState === "completed" || document.readyState === "interactive") {
	window.rendernav = rendernav(d,w);
} else {
	document.addEventListener('DOMContentLoaded',function () {window.rendernav = rendernav(d,w)});
}

function rendernav(d,w) {
	console.log('rendernav imported');
	//TARGETS FOR THE LINKS
	var plugins = colab.data.plugins;

	var navbars = d.tag('nav');//get all navbars
	
	//RENDER THE LINKS
	var list = d.createElement('ul');
	
	function onClickNavLink(target) {
		var mains = d.id('mainimport');
		for (var j = 0; j < mains.length; j++) {
			mains[j].href = target;				
		}
	}
	
	for (var i = 0; i < plugins.length; i++) {
		var listitem = d.createElement('li');
		var link = d.createElement('a');
		
			link.innerHTML = plugins[i].name;
			link.addEventListener('click',onClickNavLink.bind(undefined,i));
		
			listitem.appendChild(link);
			list.appendChild(listitem);
			
	}
	//ADD THE LINKS TO THE NAVBARS
	for (var i = 0; i < navbars.length; i++) {
		navbars[i].appendChild(list);
	}

	//RENDER UPDATES BAR
	var updatesbar = d.createElement('ul');
	updatesbar.classname = "updatesbar";
		//TODO: GETUPDATES
		var updates = ['test update one','test update 2','EMERGENCY CRITICAL UPDATE 4'];
		for (var i = 0; i < updates.length; i++) {
			var update = d.createElement('li');
				update.innerHTML = updates[i];
			updatesbar.appendChild(update);
		}	
	for (var i = 0; i < navbars.length; i++) {
		navbars[i].appendChild(updatesbar);
	}
	
	//RENDER STATUS BAR
	
	var statusbar = d.createElement('div');
	statusbar.className = "statusbar";
		//TODO: GETSTATUSBARS
		var statusbarexample = d.createElement('ul');
			var thing = d.createElement('li');
			thing.innerHTML = "TEST STATUS BAR";
		statusbarexample.appendChild(thing);
	statusbar.appendChild(statusbarexample);
	
	
	return {
		success: true 
		};
}