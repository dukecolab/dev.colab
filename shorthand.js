//Shorthand.js
//A library for simplifying the beauty that is vanilla JS
//Developed by Toolbox

var w = window;
var d = document;
d.withId = document.getElementById;
d.withClass = document.getElementsByClassName;
d.withName = document.getElementsByName;
d.withTag = document.getElementsByTagName;

d.$ = document.querySelectorAll;

d.on = document.addEventListener;
